import React from "react";
import ListStaff from "./ListStaff";
import Link from "@mui/material/Link";

const Home = () => {
  return (
    <div>
      <span>
        Code demo for:
        <Link href="https://gitlab.com/xlebenny/circleRecruitment/">
          gitlab
        </Link>
      </span>
      <div>
        <ListStaff />
      </div>
    </div>
  );
};

export default Home;
