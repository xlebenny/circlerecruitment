import React from "react";
import Paper from "@mui/material/Paper";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import TableCell from "@mui/material/TableCell";
import Table from "@mui/material/Table";
import TablePagination from "@mui/material/TablePagination";
import TableBody from "@mui/material/TableBody";
import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import DialogTitle from "@mui/material/DialogTitle";
import DialogContent from "@mui/material/DialogContent";
import DialogActions from "@mui/material/DialogActions";
import Box from "@mui/material/Box";
import MyDateTime from "../fromComponents/MyDateTime";
import MySelect from "../fromComponents/MySelect";
import MyTextField from "../fromComponents/MyTextField";
import { deepClone } from "../helpers/Helper";
import AddIcon from "@mui/icons-material/Add";
import EditIcon from "@mui/icons-material/Edit";

// TODO get from server
const statusDataset = [
  { value: "Approved", label: "Approved" },
  { value: "Pending", label: "Pending" },
  { value: "Disabled", label: "Disabled" },
];
const departmentDataset = [
  { value: 2, label: "Department1" },
  { value: 4, label: "Department2" },
];

const ListStaff = (props) => {
  const [criteria, setCriteria] = React.useState({
    status: null,
    departmentId: null,
  });
  const [pagination, setPagination] = React.useState({
    pageSize: 10,
    currentPageNumber: 1,
  });
  const [result, setResult] = React.useState({
    query: [],
    paginationMetaData: {
      totalRecord: 10,
    },
  });
  //
  const listFunc = async (criteria, pagination) => {
    const url =
      `/api/staff/list` +
      `?Criteria.Status=${criteria.status}&Criteria.DepartmentId=${criteria.departmentId}` +
      `&Pagination.CurrentPageNumber=${pagination.currentPageNumber}&Pagination.PageSize=${pagination.pageSize}`;
    //
    const result = await (await fetch(url)).json();
    return result;
  };
  const saveFunc = async (staff) => {
    const url = `/api/staff/modify`;
    await (
      await fetch(url, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(staff),
      })
    ).json();
    // TODO handle on error
    return true;
  };

  React.useEffect(() => {
    (async () => {
      // TODO: how to decouple criteria & pagination & table display field
      const result = await listFunc(criteria, pagination);
      // ASSUME: fetch have a middleware, handled all error, fail didn't call below code
      if (result) setResult(result);
    })();
  }, [criteria, pagination]);
  //
  return (
    <>
      <Criteria criteria={criteria} setCriteria={setCriteria} />
      <Display
        pagination={pagination}
        setPagination={setPagination}
        result={result}
        saveFunc={(x) => saveFunc(x)}
      />
    </>
  );
};

const Criteria = (props) => {
  return (
    <>
      <Paper>
        <Box
          component="form"
          sx={{
            "& > :not(style)": { m: 1, width: "25ch" },
          }}
        >
          <MySelect
            label="Status"
            mode="Edit"
            initialValue={props.criteria.status}
            onChange={(v) =>
              props.setCriteria((x) => {
                return { ...x, status: v };
              })
            }
            dataset={statusDataset}
          />
          <MySelect
            label="Department"
            mode="Edit"
            initialValue={props.criteria.departmentId}
            onChange={(v) =>
              props.setCriteria((x) => {
                return { ...x, departmentId: v };
              })
            }
            dataset={departmentDataset}
          />
        </Box>
      </Paper>
    </>
  );
};

const Display = (props) => {
  const [rowsPerPage, setRowsPerPage] = React.useState(10);
  //
  const [dialogViewState, setDialogViewState] = React.useState({
    isOpen: false,
    model: {},
  });
  const showDialog = (isOpen, model) => {
    if (isOpen)
      setDialogViewState({
        isOpen: true,
        model: model,
      });
    else
      setDialogViewState({
        isOpen: false,
        model: model,
      });
  };
  //
  return (
    <>
      <Paper sx={{ width: "100%", overflow: "hidden" }}>
        <TableContainer sx={{ maxHeight: 440 }}>
          <Table stickyHeader aria-label="sticky table">
            <TableHead>
              <TableRow>
                <TableCell key="employeeNumber">No</TableCell>
                <TableCell key="firstName">First Name</TableCell>
                <TableCell key="surname">Surname</TableCell>
                <TableCell key="email">Email</TableCell>
                <TableCell key="dateOfBirth">Date Of Birth</TableCell>
                <TableCell key="departmentId">Department</TableCell>
                <TableCell key="status">Status</TableCell>
                <TableCell key="edit"></TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {props.result.query.map((model) => {
                return (
                  <TableRow hover role="checkbox" tabIndex={-1} key={model.id}>
                    <TableCell key="employeeNumber">
                      <MyTextField
                        label="Employee Number"
                        mode="Plain"
                        initialValue={model?.employeeNumber}
                      />
                    </TableCell>
                    <TableCell key="firstName">
                      <MyTextField
                        label="First Name"
                        mode="Plain"
                        initialValue={model?.firstName}
                      />
                    </TableCell>
                    <TableCell key="surname">
                      <MyTextField
                        label="Surname"
                        mode="Plain"
                        initialValue={model?.surname}
                      />
                    </TableCell>
                    <TableCell key="email">
                      <MyTextField
                        label="Email"
                        mode="Plain"
                        initialValue={model?.email}
                      />
                    </TableCell>
                    <TableCell key="dateOfBirth">
                      <MyDateTime
                        label="Date of Birth"
                        mode="Plain"
                        initialValue={model?.dateOfBirth}
                        clearable={true}
                      />
                    </TableCell>
                    <TableCell key="departmentId">
                      <MySelect
                        label="Department"
                        mode="Plain"
                        initialValue={model?.departmentId}
                        dataset={departmentDataset}
                      />
                    </TableCell>
                    <TableCell key="status">
                      <MySelect
                        label="Status"
                        mode="Plain"
                        initialValue={model?.status}
                        dataset={statusDataset}
                      />
                    </TableCell>
                    <TableCell key="edit">
                      <EditIcon onClick={() => showDialog(true, model)} />
                    </TableCell>
                  </TableRow>
                );
              })}
            </TableBody>
            <caption>
              <Button
                onClick={() => showDialog(true)}
                variant="contained"
                startIcon={<AddIcon />}
              >
                Add
              </Button>
            </caption>
          </Table>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={[10, 25, 100]}
          component="div"
          count={props.result.paginationMetaData.totalRecord}
          rowsPerPage={rowsPerPage}
          // TODO backend page number and react page number not match
          page={props.pagination.currentPageNumber - 1}
          onPageChange={(event, newPage) =>
            props.setPagination((x) => {
              // TODO backend page number and react page number not match
              return { ...x, currentPageNumber: newPage + 1 };
            })
          }
          onRowsPerPageChange={(event) => {
            setRowsPerPage(event.target.value);
            props.setPagination((x) => {
              // TODO backend page number and react page number not match
              return { ...x, currentPageNumber: 0 };
            });
          }}
        />
      </Paper>
      <>
        <StaffDialog
          mode="Edit"
          isOpen={dialogViewState.isOpen}
          model={dialogViewState.model}
          onClose={() => showDialog(false)}
          onSave={(x) => props.saveFunc(x)}
        />
      </>
    </>
  );
};

const StaffDialog = (props) => {
  const [model, setModel] = React.useState({});
  const [modified, setModified] = React.useState({});
  //
  React.useEffect(() => {
    setModel(props.model);
    // setModified(JSON.parse(JSON.stringify(props.model)));
    setModified(deepClone(props.model));
  }, [props.model]);
  const onClose = () => {
    props.onClose();
  };
  const onSave = async () => {
    const isSuccess = await props.onSave(modified);
    if (isSuccess) props.onClose();
  };
  //
  const isStaffExist = !!!model;
  //
  const title = {
    Edit: (
      <>
        {isStaffExist ? `Add` : `Edit Staff Detail: ${model?.employeeNumber}`}
      </>
    ),
    Display: <>Staff Detail: {model?.employeeNumber}</>,
  };
  const context = {
    Edit: <StaffDetail mode="Edit" model={model} setModel={setModified} />,
    Display: (
      <StaffDetail mode="Display" model={model} setModel={setModified} />
    ),
  };
  const cancelButton = {
    Edit: <Button onClick={onClose}>Close</Button>,
    Display: <Button onClick={onClose}>Close</Button>,
  };
  const acceptButton = {
    Edit: <Button onClick={onSave}>Save</Button>,
    Display: <> </>,
  };
  //
  const unknownMode = <span>unknownMode: {props.mode}</span>;
  //
  return (
    <>
      <Dialog open={props.isOpen}>
        <DialogTitle>
          {title[props.mode] ? title[props.mode] : unknownMode}
        </DialogTitle>
        <DialogContent>
          {context[props.mode] ? context[props.mode] : unknownMode}
        </DialogContent>
        <DialogActions>
          {cancelButton[props.mode] ? cancelButton[props.mode] : unknownMode}
          {acceptButton[props.mode] ? acceptButton[props.mode] : unknownMode}
        </DialogActions>
      </Dialog>
    </>
  );
};

const StaffDetail = (props) => {
  const { model, setModel } = props;
  //
  const mode = props.mode;
  //
  return (
    <>
      <Box
        component="form"
        sx={{
          "& > :not(style)": { m: 1, width: "25ch" },
        }}
      >
        <MyTextField
          label="Employee Number"
          mode={mode}
          initialValue={model?.employeeNumber}
          onChange={(x) =>
            setModel((y) => {
              return { ...y, employeeNumber: x };
            })
          }
          required
        />
        <MyTextField
          label="First Name"
          mode={mode}
          initialValue={model?.firstName}
          onChange={(x) =>
            setModel((y) => {
              return { ...y, firstName: x };
            })
          }
          required
        />
        <MyTextField
          label="Surname"
          mode={mode}
          initialValue={model?.surname}
          onChange={(x) =>
            setModel((y) => {
              return { ...y, surname: x };
            })
          }
          required
        />
        <MyTextField
          label="Email"
          mode={mode}
          initialValue={model?.email}
          onChange={(x) =>
            setModel((y) => {
              return { ...y, email: x };
            })
          }
          required
        />
        <MyDateTime
          label="Date of Birth"
          mode={mode}
          initialValue={model?.dateOfBirth}
          onChange={(x) =>
            setModel((y) => {
              return { ...y, dateOfBirth: x };
            })
          }
          clearable={true}
        />
        <MySelect
          label="Department"
          mode={mode}
          initialValue={model?.departmentId}
          onChange={(x) =>
            setModel((y) => {
              return { ...y, departmentId: x };
            })
          }
          dataset={departmentDataset}
          required
        />
        <MySelect
          label="Status"
          mode={mode}
          initialValue={model?.status}
          onChange={(x) =>
            setModel((y) => {
              return { ...y, status: x };
            })
          }
          dataset={statusDataset}
          required
        />
      </Box>
    </>
  );
};

export default ListStaff;
