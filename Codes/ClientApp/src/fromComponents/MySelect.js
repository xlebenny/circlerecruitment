import React from "react";
import MenuItem from "@mui/material/MenuItem";
import TextField from "@mui/material/TextField";

const MySelect = (props) => {
  // TODO: style to Theme
  //
  const { label, mode, initialValue, onChange, dataset, required } = props;
  //
  const myDataset = dataset
    ? required
      ? dataset
      : [{ value: "", label: "--- Please Select ---" }, ...dataset]
    : [];
  const [value, setValue] = React.useState(initialValue || "");
  const onChangeFunc = (e) => {
    // TODO debounce
    onChange(e.target.value);
    setValue(e.target.value);
  };
  const displayValue = myDataset.find((x) => x.value === initialValue)?.label;
  //
  const modeDict = {
    Plain: (
      <>
        <span>{displayValue}</span>
      </>
    ),
    Display: (
      <>
        <TextField
          select
          disabled
          variant="outlined"
          label={label}
          value={value}
          onChange={onChangeFunc}
          required={required}
        >
          {myDataset.map((option) => (
            <MenuItem key={option.value} value={option.value}>
              {option.label}
            </MenuItem>
          ))}
        </TextField>
      </>
    ),
    Edit: (
      <>
        <TextField
          select
          variant="outlined"
          label={label}
          value={value}
          onChange={onChangeFunc}
          required={required}
        >
          {myDataset.map((option) => (
            <MenuItem key={option.value} value={option.value}>
              {option.label}
            </MenuItem>
          ))}
        </TextField>
      </>
    ),
  };
  //
  return modeDict[mode] ?? <>unknown mode: {mode}</>;
};

export default MySelect;
