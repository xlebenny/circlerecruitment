import React from "react";
import TextField from "@mui/material/TextField";

const MyTextField = (props) => {
  // TODO: style to Theme
  //
  const { label, mode, initialValue, onChange, required } = props;
  //
  const [value, setValue] = React.useState(initialValue || "");
  const onChangeFunc = (e) => {
    // TODO debounce
    onChange(e.target.value);
    setValue(e.target.value);
  };
  //
  const modeDict = {
    Plain: (
      <>
        <span>{initialValue}</span>
      </>
    ),
    Display: (
      <>
        <TextField
          disabled
          variant="outlined"
          label={label}
          value={value}
          onChange={onChangeFunc}
          required={required}
        />
      </>
    ),
    Edit: (
      <>
        <TextField
          variant="outlined"
          label={label}
          value={value}
          onChange={onChangeFunc}
          required={required}
        />
      </>
    ),
  };

  return modeDict[mode] ?? <>unknown mode: {mode}</>;
};

export default MyTextField;
