import React from "react";
import TextField from "@mui/material/TextField";
import DatePicker from "@mui/lab/DatePicker";

const MyDateTime = (props) => {
  // TODO: style to Theme
  //
  const { label, mode, initialValue, onChange, required, clearable } = props;
  //
  const onChangeFunc = (v) => {
    // TODO debounce
    setValue(v);
    onChange(v);
  };
  //
  const [value, setValue] = React.useState(
    initialValue === "0001-01-01T00:00:00" ? null : initialValue
  );
  const modeDict = {
    // TODO format date time
    Plain: <>{value}</>,
    Display: <>{value}</>,
    Edit: (
      <>
        <DatePicker
          variant="outlined"
          label={label}
          value={value}
          onChange={onChangeFunc}
          required={required}
          renderInput={(params) => <TextField {...params} />}
          clearable={clearable}
        />
      </>
    ),
  };
  //
  return modeDict[mode] ?? <>unknown mode: {mode}</>;
};

export default MyDateTime;
