import React from "react";
import Home from "./components/Home";
import AdapterDateFns from "@mui/lab/AdapterDateFns";
import LocalizationProvider from "@mui/lab/LocalizationProvider";

const App = () => {
  return (
    // TODO: hash routing
    <LocalizationProvider dateAdapter={AdapterDateFns}>
      <Home />
    </LocalizationProvider>
  );
};

export default App;
