export const deepClone = (obj) => {
  if (!obj) return {};
  const a = JSON.stringify(obj);
  const b = JSON.parse(a);
  return b;
};

export default { deepClone: deepClone };
