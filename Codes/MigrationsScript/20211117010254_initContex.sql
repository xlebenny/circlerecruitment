﻿IF OBJECT_ID(N'[__EFMigrationsHistory]') IS NULL
BEGIN
    CREATE TABLE [__EFMigrationsHistory] (
        [MigrationId] nvarchar(150) NOT NULL,
        [ProductVersion] nvarchar(32) NOT NULL,
        CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY ([MigrationId])
    );
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20211117010254_initContex')
BEGIN
    IF SCHEMA_ID(N'HumanResource') IS NULL EXEC(N'CREATE SCHEMA [HumanResource];');
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20211117010254_initContex')
BEGIN
    CREATE TABLE [HumanResource].[Departments] (
        [Id] bigint NOT NULL IDENTITY,
        [CreatedDate] datetime2 NOT NULL,
        [UpdatedDate] datetime2 NOT NULL,
        [Name] nvarchar(255) NOT NULL,
        CONSTRAINT [PK_Departments] PRIMARY KEY ([Id])
    );
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20211117010254_initContex')
BEGIN
    CREATE TABLE [HumanResource].[Staffs] (
        [Id] bigint NOT NULL IDENTITY,
        [CreatedDate] datetime2 NOT NULL DEFAULT (GETDATE()),
        [UpdatedDate] datetime2 NOT NULL DEFAULT (GETDATE()),
        [EmployeeNumber] nvarchar(100) NOT NULL,
        [FirstName] nvarchar(255) NOT NULL,
        [Surname] nvarchar(255) NOT NULL,
        [Email] nvarchar(255) NOT NULL,
        [DateOfBirth] datetime2 NULL,
        [DepartmentId] bigint NOT NULL,
        [Status] nvarchar(100) NOT NULL,
        CONSTRAINT [PK_Staffs] PRIMARY KEY ([Id]),
        CONSTRAINT [FK_Staffs_Departments_DepartmentId] FOREIGN KEY ([DepartmentId]) REFERENCES [HumanResource].[Departments] ([Id]) ON DELETE CASCADE
    );
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20211117010254_initContex')
BEGIN
    CREATE UNIQUE INDEX [IX_EmployeeNumber] ON [HumanResource].[Staffs] ([EmployeeNumber]);
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20211117010254_initContex')
BEGIN
    CREATE INDEX [IX_UpdatedDate] ON [HumanResource].[Staffs] ([UpdatedDate]);
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20211117010254_initContex')
BEGIN
    CREATE INDEX [IX_DepartmentId_Status] ON [HumanResource].[Staffs] ([DepartmentId], [Status]);
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20211117010254_initContex')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20211117010254_initContex', N'3.1.3');
END;

GO

