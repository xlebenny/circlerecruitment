using System;

namespace circleRecruitment.Helpers
{
    public class ErrorMessageException : Exception
    {
        public readonly string ErrorMessages;

        public ErrorMessageException(string errorMessages)
        {
            ErrorMessages = errorMessages;
        }
    }
}