using System;
using System.Linq;
using circleRecruitment.Models;

namespace circleRecruitment.Helpers
{
    public static class ExtensionMethods
    {
        public static PagedItem<T> TakePagination<T, U>(this IQueryable<T> query, PaginationParameter<U> pagination)
        {
            var result = new PagedItem<T>();
            //
            var skipRowCount = (pagination.CurrentPageNumber - 1) * pagination.PageSize;
            //
            // query
            //
            var tempQuery = query;
            pagination.OrderBy?.ForEach(x =>
            {
                switch (x.IsAscending)
                {
                    case true: tempQuery.OrderBy(y => x.FieldName); break;
                    case false: tempQuery.OrderByDescending(y => x.FieldName); break;
                    default: throw new NotSupportedException($"Not support type: {x.IsAscending}");
                }
            });
            tempQuery = tempQuery
                .Skip(skipRowCount)
                .Take(pagination.PageSize);
            //
            result.Query = tempQuery;
            //
            // meta data
            //
            var metaData = new PaginationMetaData();
            metaData.TotalRecord = query.Count();
            metaData.CurrentRowNumber = 1 + skipRowCount;
            result.PaginationMetaData = metaData;
            //
            return result;
        }
    }
}