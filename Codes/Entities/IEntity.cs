using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace circleRecruitment.Entities
{
    public abstract class IEntity
    {
        public const int StringSmallLength = 100;

        public const int StringMiddleLength = 255;

        // id should be auto generate, i hate duplicate
        // if you want a meaningful id, that should be using uniqueKey
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        [Required]
        public DateTime CreatedDate { get; set; }

        [Required]
        [Timestamp]
        public DateTime UpdatedDate { get; set; }

        protected virtual void Configure<T>(EntityTypeBuilder<T> builder) where T : IEntity
        {
            builder
                .HasIndex(nameof(IEntity.UpdatedDate))
                .HasName($"IX_{nameof(IEntity.UpdatedDate)}");
            builder
                .Property(nameof(IEntity.CreatedDate))
                .HasDefaultValueSql("GETDATE()")
                .ValueGeneratedOnAdd();
            builder
                .Property(nameof(IEntity.UpdatedDate))
                .HasDefaultValueSql("GETDATE()")
                .ValueGeneratedOnAdd();
        }
    }
}