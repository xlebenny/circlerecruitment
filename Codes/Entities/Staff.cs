using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace circleRecruitment.Entities
{
    public class Staff : IEntity, IEntityTypeConfiguration<Staff>
    {
        [Required]
        [StringLength(IEntity.StringSmallLength)]
        public string EmployeeNumber { get; set; }

        [Required]
        [StringLength(IEntity.StringMiddleLength)]
        public string FirstName { get; set; }

        [Required]
        [StringLength(IEntity.StringMiddleLength)]
        public string Surname { get; set; }

        [Required]
        [StringLength(IEntity.StringMiddleLength)]
        public string Email { get; set; }

        public DateTime? DateOfBirth { get; set; }

        // just rename simple, Table + Id
        // PS: long NOT nullable, so this is [Required] by default
        public long DepartmentId { get; set; }

        // If it can't Null, don't check == null in code
        // please keep the code clean
        public Department Department { get; set; }

        // don't, don't using 1, 2, 3, 4 or S, I in database, storage is cheap
        [Required]
        [StringLength(IEntity.StringSmallLength)]
        public StatusEnum Status { get; set; }

        public virtual void Configure(EntityTypeBuilder<Staff> builder)
        {
            base.Configure(builder);
            //
            builder
                .HasIndex(x => new { x.EmployeeNumber })
                .IsUnique()
                .HasName($"IX_{nameof(EmployeeNumber)}");

            builder
                .HasIndex(x => new { x.DepartmentId, x.Status })
                .HasName($"IX_{nameof(DepartmentId)}_{nameof(Status)}");

            builder.Property(x => x.Status)
                .HasConversion(
                    x => x.ToString(),
                    x => Enum.Parse<StatusEnum>(x)
                );
        }

        public enum StatusEnum
        {
            Approved,
            Pending,
            Disabled
        }
    }

    public class Department : IEntity, IEntityTypeConfiguration<Department>
    {
        [Required]
        [StringLength(IEntity.StringMiddleLength)]
        public string Name { get; set; }

        public void Configure(EntityTypeBuilder<Department> builder)
        {
            // do nothing
        }
    }
}
