﻿using System;
using System.Linq;
using circleRecruitment.Entities;
using circleRecruitment.Helpers;
using circleRecruitment.Models;
using Microsoft.AspNetCore.Mvc;

namespace circleRecruitment.Controllers
{
    public partial class StaffController
    {
        [HttpGet]
        public PagedItem<StaffModel> List(ListStaffModel m)
        {
            // 1. what you need to check != what show to user, e.g: password
            // 2. i have a index in circleRecruitment.Entities.Staff.Configure, this should be fast enough
            // 3. ProjectTo will generate sql like "select t.id, t.employeeNumber ... from (original sql)", if you didn't all column
            //
            // PS: below sql is capture `Sql Server Profiler`
            //     Url: /api/staff/list?Criteria.Status=Approved&Criteria.DepartmentId=2&Pagination.CurrentPageNumber=3
            //     SQL:
            //         exec sp_executesql N'
            //           SELECT COUNT(*)
            //           FROM [HumanResource].[Staffs] AS [s]
            //           WHERE ([s].[Status] = @__m_Criteria_Status_0) AND ([s].[DepartmentId] = @__m_Criteria_DepartmentId_1)
            //         '
            //         ,N'@__m_Criteria_Status_0 nvarchar(100),@__m_Criteria_DepartmentId_1 bigint'
            //         ,@__m_Criteria_Status_0=N'Approved'
            //         ,@__m_Criteria_DepartmentId_1=2
            //     SQL:
            //         exec sp_executesql N'
            //           SELECT COALESCE([s].[DateOfBirth], ''0001-01-01T00:00:00.0000000''), [s].[DepartmentId], [s].[Email], [s].[EmployeeNumber], [s].[FirstName], [s].[Id], [s].[Status], [s].[Surname]
            //           FROM [HumanResource].[Staffs] AS [s]
            //           WHERE ([s].[Status] = @__m_Criteria_Status_0) AND ([s].[DepartmentId] = @__m_Criteria_DepartmentId_1)
            //           ORDER BY (SELECT 1)
            //           OFFSET @__p_2 ROWS FETCH NEXT @__p_3 ROWS ONLY
            //         '
            //         ,N'@__m_Criteria_Status_0 nvarchar(100),@__m_Criteria_DepartmentId_1 bigint,@__p_2 int,@__p_3 int'
            //         ,@__m_Criteria_Status_0=N'Approved'
            //         ,@__m_Criteria_DepartmentId_1=2
            //         ,@__p_2=20
            //         ,@__p_3=10
            //
            var result = AutoMapper.ProjectTo<StaffModel>(
                    HumanResourceContext.Staffs
                        .Where(x =>
                            (m.Criteria.Status == null || x.Status == m.Criteria.Status)
                            && (m.Criteria.DepartmentId == null || x.DepartmentId == m.Criteria.DepartmentId)
                        )
                )
                // TIPS: user only sort what they see
                // TODO: default page count maybe user/global setting
                .TakePagination(m.Pagination);
            //
            return result;
        }

        [HttpPost]
        public void Modify([FromBody] StaffModifation parameter)
        {
            var staff = HumanResourceContext.Staffs
                .Where(x => parameter.Id != null && x.Id == parameter.Id)
                .SingleOrDefault();
            var isExist = (staff != null);

            //
            // validation
            //   1. in EACH end point have difference validation, ever it modify same entity, so CAN'T generic
            //      1.1 but some common logic may extract, e.g: entity field level validation
            //   2. keep it simple stupid - because announcement [Required] is so hard to customize
            //   3. remember, user can call without browser (crul / postman), so we need to check EVERYTHING
            //
            if (parameter.EmployeeNumber == null || parameter.EmployeeNumber.Trim() == "") throw new ErrorMessageException("Please input Employee Number");
            if (parameter.FirstName == null || parameter.FirstName.Trim() == "") throw new ErrorMessageException("Please input First Name");
            if (parameter.Surname == null || parameter.Surname.Trim() == "") throw new ErrorMessageException("Please input Surname Name");
            // didn't suppose user will submit wrong DepartmentId,
            //   so no this human readable message
            //   any wrong FK will guard by database
            if (parameter.DepartmentId == null) throw new ErrorMessageException("Please input Department");
            if (parameter.Status == null || parameter.Status.Trim() == "") throw new ErrorMessageException("Please input Status");

            //
            // save logic
            //

            if (isExist) AutoMapper.Map(parameter, staff);
            else HumanResourceContext.Staffs.Add(AutoMapper.Map<Staff>(parameter));
            //
            HumanResourceContext.SaveChanges();
        }

        [HttpPost]
        public void Delete([FromBody] StaffDeleteation parameter)
        {
            var staff = HumanResourceContext.Staffs
                .Where(x => x.Id == parameter.Id)
                .SingleOrDefault();
            var isExist = (staff != null);

            //
            // validation
            //
            if (isExist == false) throw new ErrorMessageException("Staff not exist");

            //
            // save logic
            //
            HumanResourceContext.Staffs.Remove(staff);
            //
            HumanResourceContext.SaveChanges();
        }
    }
}
