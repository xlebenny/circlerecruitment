﻿using AutoMapper;
using circleRecruitment.Entities;
using Microsoft.AspNetCore.Mvc;

namespace circleRecruitment.Controllers
{
    public partial class StaffController : ControllerBase
    {
        protected readonly IMapper AutoMapper;

        protected readonly HumanResourceContext HumanResourceContext;

        public StaffController(IMapper autoMapper, HumanResourceContext humanResourceContext)
        {
            AutoMapper = autoMapper;
            HumanResourceContext = humanResourceContext;
        }
    }
}
