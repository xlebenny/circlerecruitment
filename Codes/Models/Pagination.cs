using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace circleRecruitment.Models
{
    public class PaginationParameter<T>
    {
        public int CurrentPageNumber { get; set; } = 1;
        public int PageSize { get; set; } = 10;
        public List<OrderByConfig> OrderBy { get; set; } = new List<OrderByConfig>();

        [Required]
        public T Parameter { get; set; }

        public class OrderByConfig
        {
            public string FieldName { get; set; }
            public bool IsAscending { get; set; }
        }
    }

    public class PaginationResult<T>
    {
        public PaginationMetaData PaginationMetaData { get; set; }
        public List<T> Result { get; set; }
    }

    public class PaginationMetaData
    {
        public int TotalRecord { get; set; }

        public int CurrentRowNumber { get; set; } // n of m record
    }

    public class PagedItem<T>
    {
        public PaginationMetaData PaginationMetaData { get; set; }

        public IQueryable<T> Query { get; set; }
    }
}