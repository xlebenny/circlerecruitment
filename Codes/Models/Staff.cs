using System;
using AutoMapper;

namespace circleRecruitment.Models
{
    public class ListStaffModel
    {
        public _Criteria Criteria { get; set; } = new _Criteria();

        public PaginationParameter<StaffModel> Pagination { get; set; } = new PaginationParameter<StaffModel>();

        public class _Criteria
        {
            // user may not input
            public Entities.Staff.StatusEnum? Status { get; set; }

            // user may not input
            // PS: The Question having said search by name or id
            public long? DepartmentId { get; set; }
        }
    }

    [AutoMap(typeof(Entities.Staff), ReverseMap = true)]
    public class StaffModel
    {
        public long Id { get; set; }

        public string EmployeeNumber { get; set; }

        public string FirstName { get; set; }

        public string Surname { get; set; }

        public string Email { get; set; }

        public DateTime DateOfBirth { get; set; }

        public long DepartmentId { get; set; }

        public string Status { get; set; }
    }

    [AutoMap(typeof(Entities.Staff), ReverseMap = true)]
    public class StaffModifation
    {
        public long? Id { get; set; }

        public string EmployeeNumber { get; set; }

        public string FirstName { get; set; }

        public string Surname { get; set; }

        public string Email { get; set; }

        public DateTime? DateOfBirth { get; set; }

        public long? DepartmentId { get; set; }

        public string Status { get; set; }
    }

    [AutoMap(typeof(StaffModel), ReverseMap = true)]
    public class StaffDeleteation
    {
        public long Id { get; set; }
    }
}