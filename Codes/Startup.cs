using AutoMapper;
using circleRecruitment.Entities;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.SpaServices.ReactDevelopmentServer;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace circleRecruitment
{
    public class Startup
    {
        public Startup(IWebHostEnvironment environment, IConfiguration configuration)
        {
            Environment = environment;
            Configuration = configuration;
        }

        public IWebHostEnvironment Environment { get; }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // services.AddAutoMapper(new System.Reflection.Assembly[]{
            //     typeof(circleRecruitment.Models.PagedItem<>).Assembly,
            //     typeof(circleRecruitment.Entities.IEntity).Assembly,
            // });
            // services.AddAutoMapper(
            //     System.Reflection.Assembly.GetExecutingAssembly()
            // );
            services.AddAutoMapper(typeof(Startup));
            //
            services.AddDbContext<HumanResourceContext>(options =>
            {
                var connectionString = Configuration.GetConnectionString(nameof(HumanResourceContext));
                var modifedConnectionString = $"{connectionString}; Application Name='{System.Environment.MachineName} - {Environment.ApplicationName}';";
                options.UseSqlServer(modifedConnectionString);
            });

            services
                .AddControllers(x =>
                {
                    //  x.Filters.Add<MyAuditLog>();
                })
                .AddNewtonsoftJson(options =>
                {
                    options.SerializerSettings.Converters.Add(new Newtonsoft.Json.Converters.StringEnumConverter());
                    options.SerializerSettings.Formatting = Newtonsoft.Json.Formatting.Indented;
                    // options.SerializerSettings.TypeNameHandling = Newtonsoft.Json.TypeNameHandling.Objects;
                });

            // In production, the React files will be served from this directory
            services.AddSpaStaticFiles(configuration =>
            {
                configuration.RootPath = "ClientApp/build";
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseSpaStaticFiles();

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "api",
                    pattern: "api/{controller}/{action}/{id?}"
                );
            });

            app.UseSpa(spa =>
            {
                spa.Options.SourcePath = "ClientApp";

                if (env.IsDevelopment())
                {
                    spa.UseReactDevelopmentServer(npmScript: "start");
                }
            });
        }
    }
}
