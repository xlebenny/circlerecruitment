#!/bin/bash

currentPath="$(pwd)"

cd ./Codes
dotnet publish -c Release -o "../_temp/"

cd ${currentPath}