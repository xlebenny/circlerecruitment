# README

[![pipeline status](https://gitlab.com/xlebenny/circlerecruitment/badges/master/pipeline.svg)](https://gitlab.com/xlebenny/circlerecruitment/-/commits/master)

Online Demo: [HERE](https://circlerecruitment.meowmeowshop.dev/)

## Practices
- Using variable / method name to descript what i do
    - comment it used to explain why "strange", e.g: return -2
    - or split difference code block
- 2 type of method
    1. low-level by SRP
    2. high-level by OCP, combination with few low-level method, concept like design pattern 'Client' or abstract factory pattern "meal deal" example.
- Models vs Entity
    1. Model is user "view", just select what user will "see", and don't show shouldn't to know (e.g: password)

## TODO
- backend result wrapper (for generic handle error message)
- fontend error handling
- after "save" / "add" refresh the page

## Functional Requirements
- Human Resource List Screen
    - Frontend: completed
    - Backend: completed
- Human Resource Add/Edit Screen
    - Frontend: INCOMPLETE
    - Backend: completed - haven't test yet

## Non-Functional Requirements
- Performance vs Code First
    If you have double check what Entity Framework generated,
    Then refine the entity, it's doesn't matter using Database First / Code First
    But some practices can make EntityFramework/SQL effective
        1. Index what you will search, prevent full table scan
            - Index may effect insert/update performance, but except bulk insert, major system Select case more then insert/update, maybe 80%/20% 
        2. Search once, then join all by PK
            - So auto inc id is useful
        3. Rebuild index weekly
        4. No firstName + ' ' + lastName like '%abc%', this can't be index, if really need it, consider trigger/day end generate some "view table"/summary table
        5. Don't join few to tooMany, consider to split two request, then join in frontend (e.g: staff --> department)
        6. Beware IQueryable, it will run SQL request each time, if you want do some handling after query, reminder .ToList()


- The app code should be simple and easy to read.
    - KISS, except code can be re-use (then extract to Helper)

- horizontal scaling
    - Haven't any session/any variable storage in memory, if have a load balancer, this can be scaling out
